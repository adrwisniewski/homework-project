#!/usr/bin/env python3


# Homework assignment slide 37

# function printing name of the home assignment author
def my_name():
    print("My name is Adrian Wisniewski")


my_name()


# function which adds up 3 numbers
def addition(x, y, z):
    return x + y + z


add = addition(1, 2, 3)
print(add)


# function, which prints "Hello World!"
def hello():
    return "Hello World"


hi = hello()
print("{}!".format(hi))


# function subtracting one number from another
def subtraction(a, b):
    return a - b


sub = subtraction(10, 9)
print(sub)


# Homework assignment slide 46

# Defined class with two attributes besides self and a method exist()
class Object:
    def __init__(self, obj_name="Rectangle", obj_shape="rectangular"):
        print("Creating rectangular object")
        self.name = obj_name
        self.shape = obj_shape

    def exist(self):
        print("The object exists")


# function which creates an object
def create_object():
    return Object()


obj = create_object()
print(obj)
obj.exist()


# subclass Square which inherits methods and instance attributes from class Object
class Square(Object):
    def __init__(self, sq_name="Square"):
        print("Creating square")
        self.name = sq_name

    def corners(self):
        print("I'm a square and I have 4 corners!")


sq = Square()
print(sq)
sq.exist()
sq.corners()
print("Even though I created a {} \nits shape is still {}".format(sq.name, obj.shape))
